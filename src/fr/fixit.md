# Répare-le !

Lire et comprendre les avertissements et messages d’erreur de l’interpréteur est très utile. C’est pourquoi je te propose ici une série de mini-exercices dans lesquels je te propose un code cassé qu’il faudra réparer d’après ce que te retourne l’interpréteur.

## Exercice n°1

```scheme
(define congratulations "Well done !")
(display congralutations)
```

L'évaluation de ces expressions retourne :
```
;;; <unknown-location>: warning: possibly unbound variable `congralutations'
ice-9/boot-9.scm:1685:16: In procedure raise-exception:
Unbound variable: congralutations

Entering a new prompt.  Type `,bt' for a backtrace or `,q' to continue.
```
