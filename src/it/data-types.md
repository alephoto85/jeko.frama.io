# Tipi di dati

La reference del linguaggio elenca non meno di 23 tipi di dati (e questo non è nemmeno esaustivo), dal più semplice al più complesso.

Ognuno di questi tipi ha una propria serie di rappresentazioni e funzionalità.

Alcuni di questi tipi sono abbastanza comuni nei linguaggi di programmazione (booleani, numeri, stringhe). Altri sono più esotici (simboli).

Nei sottocapitoli che seguono, implementerò in modo rapido e semplice questi tipi di dati a titolo illustrativo (non tratterò tutte le loro caratteristiche).
