# Fix it!

Reading and understanding the warnings and error messages of the interpreter is very useful. That's why I invite you here in a serie of mini-exercises in which I challenge you with a broken code that you will have to repair according to what the interpreter returns to you.

## Exercise n°1

```scheme
(define congratulations "Well done !")
(display congralutations)
```

Evaluating these expressions returns:
```
;;; <unknown-location>: warning: possibly unbound variable `congralutations'
ice-9/boot-9.scm:1685:16: In procedure raise-exception:
Unbound variable: congralutations

Entering a new prompt.  Type `,bt' for a backtrace or `,q' to continue.
```
