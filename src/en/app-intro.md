# Creating an application

The following chapters will guide you through the development of a simple application with Guile.

Like the first chapters, I propose to approach the exercise through development practices and methods that I appreciate a lot (Test Driven Development, Clean Code, Clean Architecture).

Firstly, you will make sure that your Guile installation works.

Secondly, you will implement the first use cases of the application with their command line interface.

Thirdly, you will learn how to take advantage of third party libraries to extend your program (to propose a graphical and web interface for example). You will also learn how to manage dependencies and distribute your program using a package manager.

> **Prerequisites:** This tutorial does not replace an introduction to programming and expects you to be familiar with some basic concepts. You should be comfortable using the command line. If you already know how to program in other languages, this tutorial can be a good first contact with Guile.
>
> **Getting help:** If at any time you feel overwhelmed or confused by a feature you are using, take a look at [the official language reference](https://www.gnu.org/software/guile/manual/html_node/index.html). It is distributed with most Guile installations (accessible from a terminal: `info guile`).

So, what kind of application will you create? How about a shopping list application? A small utility to which we would give as inputs :
* a flag to indicate whether to add (`--add`) or remove (`--remove`) an item from the list;
* the item to add to the list (`tomatoes`) or the index of the item to remove (`12`).

And which would return the contents of the list in the absence of an argument:
```
$ grocery-list
1. Quinoa
2. Mushrooms
3. Rice
4. Lentils
5. Spinach
```
> Note: This tutorial is written for Guile 3. The code examples can be used in all versions of Guile 3.
